const Koa = require('koa')
const app = new Koa()

require('dotenv').config()

app.use(require('./routes/ping').routes())
app.use(require('./routes/files').routes())

app.listen(process.env.STATIC_API_PORT, () => {
  const message = `Server listening on ${process.env.STATIC_API_IP}:${process.env.STATIC_API_PORT}`
  console.log(message)
})

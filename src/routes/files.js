const upload = require('@koa/multer')()
const router = require('koa-router')({
  prefix: '/files',
  sensitive: true
})

const filesController = require('../controllers/files')

router.post('/', upload.array('files', 3), filesController.uploadFiles)
router.delete('/:id', filesController.deleteFileById)

module.exports = router

const router = require('koa-router')({ sensitive: true })
const pingController = require('../controllers/ping')

router.get('/ping', pingController.pong)

module.exports = router

module.exports = {
  uploadFiles,
  deleteFileById
}

const fs = require('fs')
const axios = require('axios')
const { resolve } = require('path')
const shortid = require('shortid')
const { existsSync } = require('fs')
const mimetypes = ['image/png', 'image/jpeg', 'image/webp']

async function uploadFiles(ctx) {
  try {
    let { files = [], query = {} } = ctx.request
    let { productIds } = query

    if (!files.length) {
      onCatch(ctx, 'No files to upload')

      return
    }

    for (const file of files) {
      const { mimetype } = file

      if (mimetypes.indexOf(mimetype) === -1) {
        onCatch(ctx, `Unsupported mimetype ${mimetype}`)

        return
      }
    }

    files = files.map((el) => {
      const { buffer, mimetype } = el
      let ext = mimetype.split('/')[1]
      ext = ext === 'jpeg' ? 'jpg' : ext

      let fileId, targetDir

      do {
        fileId = shortid.generate()
        targetDir = getTargetDir(fileId)
      } while (existsSync(targetDir))

      fs.mkdirSync(targetDir, { recursive: true })

      const originPath = `${targetDir}/origin.${ext}`
      const url = `${process.env.HOST}/${fileId.slice(-1)}/${fileId.slice(-2)}/${fileId}/origin.${ext}`

      fs.writeFileSync(originPath, buffer)

      return url
    })

    let url = `${process.env.API_HOST}/files/urls`
    if (productIds) {
      url += `?productIds=${productIds}`
    }

    axios.post(url, files).catch((reason) => {
      console.error(reason)
    })

    ctx.body = files
  } catch (e) {
    onCatch(ctx, e)
  }
}

function deleteFileById(ctx) {
  try {
    const { id: fileId } = ctx.params
    const targetDir = getTargetDir(fileId)

    if (existsSync(targetDir)) {
      fs.rmSync(targetDir, { recursive: true })
      const url = `${process.env.API_HOST}/files/delete-by-dirs`
      axios.post(url, [ fileId ]).catch((reason) => console.error(reason))
      ctx.body = { status: 200 }

      return
    }

    onCatch(ctx, 'Directory not found', 404)
  } catch (e) {
    onCatch(ctx, e)
  }
}

function getTargetDir(fileId) {
  return resolve(
    __dirname,
    '..',
    '..',
    '..',
    `files/${fileId.slice(-1)}/${fileId.slice(-2)}/${fileId}`
  )
}

function onCatch(ctx, e, status = 400) {
  console.error(e)

  ctx.status = status
  ctx.body = {
    status: status,
    reason: e.toString()
  }
}

require('dotenv').config()
const request = require('supertest')(process.env.HOST)
const contentType = [
  'text/plain; charset=utf-8',
  'application/json; charset=utf-8',
]

describe('HEAD /ping', () => {
  it('got 200 "OK"', (done) => {
    request.head('/ping')
      .expect(200, done)
  })
})

describe('GET /ping', () => {
  it('got 200 "OK"', (done) => {
    request.get('/ping')
      .expect(200, done)
  })
  it('response body got "pong"', (done) => {
    request.get('/ping')
      .expect('pong', done)
  })
  it('Content-type got "text/plain; charset=utf-8"', (done) => {
    request.get('/ping')
      .expect('Content-type', contentType[0], done)
  })
})

describe('HEAD /pinG', () => {
  it('got 404 "Not Found"', (done) => {
    request.head('/pinG')
      .expect(404, done)
  })
})

describe('GET /pinG', () => {
  it('got 404 "Not Found"', (done) => {
    request.get('/pinG')
      .expect(404, done)
  })
})
